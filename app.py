from flask import Flask, render_template, request
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer


app = Flask(__name__)

bot = ChatBot(
    'Hadita',
    storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
    logic_adapters=[
        'chatterbot.logic.BestMatch'
    ],
    database_uri='mongodb://localhost:27017/chatterbot-database'
)

trainer = ChatterBotCorpusTrainer(bot)

trainer.train(
    "./corpus/"
)


@app.route('/')
def home():
    return render_template("home.html")


@app.route('/get')
def get_bot_response():
    user_text = request.args.get("msg")
    return str(bot.get_response(user_text))


if __name__ == '__main__':
    app.run()
